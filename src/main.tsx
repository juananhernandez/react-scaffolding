import React from 'react'
import ReactDOM from 'react-dom/client'
import {
  createBrowserRouter,
  RouterProvider,
} from 'react-router-dom'
import { Home } from './components/Home'
import { Api } from './components/Api'


const router = createBrowserRouter([
  {
    path: '/',
    element: <Home />
  },
  {
    path: '/users',
    element: <Api />
  },
])

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
