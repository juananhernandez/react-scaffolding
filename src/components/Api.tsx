import axios from "axios"
import { useEffect, useState } from "react"

export function Api () {

    const [users, setUsers] = useState<{[fieldName: string]: string}[]>([]);

    useEffect(() => {
        axios.get('api/users')
        .then(response => {
            setUsers(response.data)
        })
        .catch(error => {
            console.log(error)
            const mockUsers = [{'backend': 'notWorking'}]
            setUsers(mockUsers)
        })
    }, [])

    return (
        <div>
            {users.map(user => (
                <p>{JSON.stringify(user)}</p>
            ))}
        </div>
    ) 
}